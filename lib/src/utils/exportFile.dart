import 'dart:io';

import 'package:csv/csv.dart';
import 'package:pedometer_app/src/models/DataAccelerometer.dart';
// import 'package:simple_permissions/simple_permissions.dart';
import 'package:path_provider/path_provider.dart' as _platform;

getCsv(List<DataAccelerometer> associateList, String name) async {
  //create an element rows of type list of list. All the above data set are stored in associate list
//Let associate be a model class with attributes name,gender and age and associateList be a list of associate model class.

  List<List<dynamic>> rows = List<List<dynamic>>();
  for (int i = 0; i < associateList.length; i++) {
//row refer to each column of a row in csv file and rows refer to each row in a file
    List<dynamic> row = List();
    row.add(associateList[i].time);
    row.add(associateList[i].x);
    row.add(associateList[i].y);
    row.add(associateList[i].z);
    rows.add(row);
  }

  // await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
  // bool checkPermission =
  //     await SimplePermissions.checkPermission(Permission.WriteExternalStorage);
  // if (checkPermission) {
//store file in documents folder

  String dir =
      (await getExternalStorageDirectory()).absolute.path + "/documents";
  var file = "$dir";
  print(" FILE " + file);
  File f = new File(file + "$name.csv");

// convert rows to String and write as csv file

  String csv = const ListToCsvConverter().convert(rows);
  f.writeAsString(csv);
  // }
}

Future<Directory> getExternalStorageDirectory() async {
  final path = await _platform.getExternalStorageDirectory();
  if (path == null) {
    return null;
  }
  return path;
}
