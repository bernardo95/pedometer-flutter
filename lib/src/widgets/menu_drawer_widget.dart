import 'package:flutter/material.dart';

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/menu-img.jpeg'), fit: BoxFit.cover),
            ),
            child: null,
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.green),
            title: Text(
              'Pedometer',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            onTap: () {
              Navigator.pushNamed(context, 'home');
            },
          ),
          ListTile(
            leading: Icon(Icons.graphic_eq, color: Colors.green),
            title: Text(
              'Accelerometer',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            onTap: () {
              Navigator.pushNamed(context, 'accelerometer');
            },
          ),
          ListTile(
            leading: Icon(Icons.person, color: Colors.green),
            title: Text(
              'User Accelerometer',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            onTap: () {
              Navigator.pushNamed(context, 'accelerometer_user');
            },
          ),
        ],
      ),
    );
  }
}
