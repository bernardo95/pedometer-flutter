import 'package:flutter/material.dart';

class DataAccelerometer {
  final String time;
  final double x;
  final double y;
  final double z;

  DataAccelerometer(
      {@required this.x,
      @required this.y,
      @required this.z,
      @required this.time});
}
