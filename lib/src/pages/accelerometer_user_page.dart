import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pedometer_app/src/models/DataAccelerometer.dart';
import 'package:pedometer_app/src/utils/exportFile.dart';
import 'package:sensors/sensors.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class AccelerometerUserPage extends StatefulWidget {
  AccelerometerUserPage({Key key}) : super(key: key);

  @override
  _AccelerometerUserPageState createState() => _AccelerometerUserPageState();
}

class _AccelerometerUserPageState extends State<AccelerometerUserPage> {
  int time = 0;
  int limit = 800;
  bool play = true;

  List<DataAccelerometer> dataGraphic = [];
  List<DataAccelerometer> dataExport = [];

  List<double> _userAccelerometerValues;
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];
  @override
  void dispose() {
    super.dispose();
    this.cancelSubscription();
  }

  @override
  void initState() {
    super.initState();
  }

  void initSubscription() {
    _streamSubscriptions
        .add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      setState(() {
        _userAccelerometerValues = <double>[event.x, event.y, event.z];
        dataGraphic.add(new DataAccelerometer(
            x: event.x, y: event.y, z: event.z, time: (time++).toString()));
        if (dataGraphic.length > limit) {
          dataGraphic.removeRange(
              dataGraphic.length - limit, dataGraphic.length);
        }
      });
    }));
  }

  void cancelSubscription() {
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<String> userAccelerometer = _userAccelerometerValues
        ?.map((double v) => v.toStringAsFixed(1))
        ?.toList();

    return Scaffold(
      appBar: AppBar(
        title: const Text('UserAccelerometer Sensor'),
        actions: [
          Container(
              padding: EdgeInsets.only(right: 10.0),
              child: play
                  ? IconButton(
                      onPressed: () => {
                        setState(() {
                          this.play = !this.play;
                          this.initSubscription();
                        })
                      },
                      icon: Icon(Icons.play_arrow),
                    )
                  : IconButton(
                      onPressed: () async => {
                        setState(() {
                          this.play = !this.play;
                          this.cancelSubscription();
                        }),
                        await getCsv(this.dataExport, "accelerometer_user")
                      },
                      icon: Icon(Icons.stop),
                    ))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                // Chart title
                title:
                    ChartTitle(text: 'X Axis signal useraccelerometer values'),
                // Enable legend
                legend: Legend(isVisible: true),
                // Enable tooltip
                tooltipBehavior: TooltipBehavior(enable: true),
                series: <ChartSeries<DataAccelerometer, String>>[
                  LineSeries<DataAccelerometer, String>(
                      color: Colors.blue,
                      dataSource: dataGraphic,
                      xValueMapper: (DataAccelerometer dataX, _) => dataX.time,
                      yValueMapper: (DataAccelerometer dataX, _) => dataX.x,
                      name: 'Signal value',
                      // Enable data label
                      dataLabelSettings: DataLabelSettings(isVisible: true))
                ]),
            SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                // Chart title
                title:
                    ChartTitle(text: 'Y Axis signal useraccelerometer values'),
                // Enable legend
                legend: Legend(isVisible: true),
                // Enable tooltip
                tooltipBehavior: TooltipBehavior(enable: true),
                series: <ChartSeries<DataAccelerometer, String>>[
                  LineSeries<DataAccelerometer, String>(
                      color: Colors.red,
                      dataSource: dataGraphic,
                      xValueMapper: (DataAccelerometer dataY, _) => dataY.time,
                      yValueMapper: (DataAccelerometer dataY, _) => dataY.y,
                      name: 'Signal value',
                      // Enable data label
                      dataLabelSettings: DataLabelSettings(isVisible: true))
                ]),
            SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                // Chart title
                title:
                    ChartTitle(text: 'Z Axis signal useraccelerometer values'),
                // Enable legend
                legend: Legend(isVisible: true),
                // Enable tooltip
                tooltipBehavior: TooltipBehavior(enable: true),
                series: <ChartSeries<DataAccelerometer, String>>[
                  LineSeries<DataAccelerometer, String>(
                      color: Colors.purple,
                      dataSource: dataGraphic,
                      xValueMapper: (DataAccelerometer dataZ, _) => dataZ.time,
                      yValueMapper: (DataAccelerometer dataZ, _) => dataZ.z,
                      name: 'Signal value',
                      // Enable data label
                      dataLabelSettings: DataLabelSettings(isVisible: true))
                ]),
            Padding(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('UserAccelerometer: $userAccelerometer'),
                ],
              ),
              padding: const EdgeInsets.all(16.0),
            ),
          ],
        ),
      ),
    );
  }
}
