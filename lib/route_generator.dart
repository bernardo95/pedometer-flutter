import 'package:flutter/material.dart';
import 'package:pedometer_app/main.dart';
import 'package:pedometer_app/src/pages/accelerometer_page.dart';
import 'package:pedometer_app/src/pages/accelerometer_user_page.dart';
import 'package:pedometer_app/src/pages/home_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case 'app':
        return MaterialPageRoute(builder: (_) => MyApp());
      case 'home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case 'accelerometer':
        return MaterialPageRoute(builder: (_) => AccelerometerPage());
      case 'accelerometer_user':
        return MaterialPageRoute(builder: (_) => AccelerometerUserPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
